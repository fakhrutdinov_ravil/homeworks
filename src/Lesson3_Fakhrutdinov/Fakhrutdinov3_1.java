package Lesson3_Fakhrutdinov;

import java.util.Arrays;
import java.util.Scanner;

public class Fakhrutdinov3_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива и нажмите Enter");
        int array[] = new int[scanner.nextInt()];
        System.out.println("Введите числа массива через Enter");
        for (int i=0; i<array.length; i++)
            array[i]=scanner.nextInt();
        System.out.println("Наш массив: "+Arrays.toString(array));

        System.out.println(summArray(array)); //Ввести на экран сумму элементов массива
        arrayFlip(array); //Переворот массива заданного в параметр
        System.out.println(arrayAverage(array)); //Вывод среднеарифметического
        arrayRotate(array); //Максимальный и минимальный элемент в массиве меняюстя местами
        sortBubble(array); //Сортировка методом пузырька
        System.out.println(tranformationArray(array)); //Перевод массива в число

    }

    public static int summArray(int[] array) {
        System.out.println("Сумма элементов массива:");
        int summ = 0;
        for (int i = 0; i < array.length; i++) {
            summ = summ + array[i];
        }
        return summ;
    }

    public static void arrayFlip(int[] array) {
        System.out.println("Зеркальное отображение массива");
        int newArray[] = Arrays.copyOf(array, array.length);
        int arrayCopy[]=Arrays.copyOf(array, array.length);
        int arrayLength=array.length-1;
        for (int i = 0; i < array.length; i++, arrayLength--) {
            newArray[i] = arrayCopy[arrayLength];
        }
        System.out.println(Arrays.toString(newArray));
    }

    public static int arrayAverage(int array[]) {
        System.out.println("Среднее арифметическое");
        int summ = 0;
        for (int i = 0; i < array.length; i++) {
            summ = summ + array[i];
        }
        int Average = summ / (array.length - 1);
        return Average;
    }

    public static void arrayRotate(int[] array) {
        System.out.println("Меняем максимальный и минимальный элемент местами");
        int max = array[0];
        int min = array[0];
        int positionOfMax = 0;
        int positionOfMin = 0;
        int temp;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                positionOfMax = i;
            }
            if (array[i] < min) {
                min = array[i];
                positionOfMin = i;
            }
        }
        temp = array[positionOfMax];
        array[positionOfMax] = array[positionOfMin];
        array[positionOfMin] = temp;
        System.out.println(Arrays.toString(array));
    }

    public static void sortBubble(int[] array) {
        System.out.println("Сортировка методом пузырька: ");
        int temp, max;
        max = array[0];
        for (int i = 0; i < (array.length - 1) * (array.length - 1); i++) {
            for (int n = 0; n < array.length - 1; n++) {
                if (array[n + 1] < array[n]) {
                    temp = array[n];
                    array[n] = array[n + 1];
                    array[n + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static long tranformationArray(int[] array) {
        System.out.println("Перевод массива в число");
        long number = 0;
        int arrayCopy[]= Arrays.copyOf(array, array.length);
        int rank;
        for (int i=0; i<array.length; i++) {
            rank=1;
            for (;arrayCopy[i]>0;arrayCopy[i]/=10) {
                rank*=10;
            }
            number=number*rank+array[i];
        }
        return number;
    }
}
