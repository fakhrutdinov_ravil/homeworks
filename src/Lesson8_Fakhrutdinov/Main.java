package Lesson8_Fakhrutdinov;

public class Main {
    public static void main(String[] args) {
        GeometricFigures Circle = new Circle(10);
        GeometricFigures Ellipse = new Ellipse(10, 15);
        GeometricFigures Rectangle = new Rectangle(10, 15);
        GeometricFigures Square = new Square(10);

        Print(Circle, Ellipse, Rectangle, Square);

        Circle.move(5, 15);
        Ellipse.move(5, 15);
        Rectangle.move(5, 15);
        Square.move(5, 15);

        Print(Circle, Ellipse, Rectangle, Square);

        Circle.rescale(2);
        Ellipse.rescale(2);
        Rectangle.rescale(2);
        Square.rescale(2);

        Print(Circle, Ellipse, Rectangle, Square);

    }

    private static void Print(GeometricFigures Circle, GeometricFigures Ellipse, GeometricFigures Rectangle, GeometricFigures Square) {
        System.out.println(Circle.toString());
        System.out.println(Ellipse.toString());
        System.out.println(Rectangle.toString());
        System.out.println(Square.toString());
        System.out.println();
    }


}
