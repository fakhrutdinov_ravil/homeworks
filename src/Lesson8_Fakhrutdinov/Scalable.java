package Lesson8_Fakhrutdinov;

public interface Scalable {
    void rescale(double x);
}
