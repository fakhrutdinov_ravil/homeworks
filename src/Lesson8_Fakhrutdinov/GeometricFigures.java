package Lesson8_Fakhrutdinov;

public abstract class GeometricFigures implements Scalable, Relocatable {
    Point center;
    protected double side1;
    protected double side2;

    public GeometricFigures(Point center, double side1, double side2) {
        this.center = center;
        this.side1 = side1;
        this.side2 = side2;
    }

    public GeometricFigures(double side1, double side2) {
        this.center = new Point();
        this.side1 = side1;
        this.side2 = side2;
    }

    public abstract double area();

    public abstract double perimeter();


    @Override
    public void rescale(double x) {
        this.side1 *= x;
        this.side2 *= x;
    }

    @Override
    public void move(double x, double y) {
        center.setX(x);
        center.setY(y);
    }

    @Override
    public void move(Point point) {
        move(point.getX(), point.getY());
    }

    @Override
    public String toString() {
        return "GeometricFigures{" +
                "center=" + center +
                ", side1=" + side1 +
                ", side2=" + side2 +
                '}';
    }
}
