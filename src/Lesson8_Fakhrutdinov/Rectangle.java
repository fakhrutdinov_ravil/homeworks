package Lesson8_Fakhrutdinov;

public class Rectangle extends GeometricFigures {
    public Rectangle(Point center, double side1, double side2) {
        super(center, side1, side2);
    }

    public Rectangle(double side1, double side2) {
        super(side1, side2);
    }

    @Override
    public double area() {
        return side1 * side2;
    }

    @Override
    public double perimeter() {
        return (2 * side1) + (2 * side2);
    }
}
