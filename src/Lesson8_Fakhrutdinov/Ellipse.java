package Lesson8_Fakhrutdinov;

public class Ellipse extends GeometricFigures {
    //Конструтор с заданной точкой
    public Ellipse(Point center, double side1, double side2) {
        super(center, side1, side2);
    }

    //Конструтор без точки (Точка создается по дефолту)
    public Ellipse(double side1, double side2) {
        super(side1, side2);
    }

    @Override
    public double area() {
        return (Math.PI * side1 * side2);
    }

    @Override
    public double perimeter() {
        return 4 * (((Math.PI * side1 * side2) + Math.pow(side1 - side2, 2.0)) / side1 + side2);
    }
}
