package Lesson8_Fakhrutdinov;

public class Square extends Rectangle {
    public Square(Point center, double side1) {
        super(center, side1, side1);
    }

    public Square(double side1) {
        super(side1, side1);
    }
}
