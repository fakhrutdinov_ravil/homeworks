package Lesson14_Fakhrutdinov;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class MainShortVersion {
    public static List<Car> stringCarFromCar() {
        BufferedReader bufferedReader;
        String line;

        List<Car> carList = new ArrayList<>();

        Function<String, Car> getCarFromString = (Function<String, Car>) s -> {
            String[] parts = s.split("\\|");
            String car_number = parts[0];
            String car_name_factory = parts[1];
            String model = parts[2];
            String color = parts[3];
            String car_mileage = parts[4];
            String price = parts[5];
            return new Car(car_number, model, color, Integer.parseInt(car_mileage), Integer.parseInt(price));
        };

        try {
            bufferedReader = new BufferedReader(new FileReader("cars.txt"));
            line = bufferedReader.readLine();
            for (int lineCar = 0; line != null; lineCar++) {
                Car currentCar = getCarFromString.apply(line);
                carList.add(currentCar);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            throw new IllegalStateException("Файл не найден");
        }
        return carList;
    }

    public static void main(String[] args) {
        List<Car> cars = stringCarFromCar();
        Stream<Car> carStream1 = cars.stream();
        Stream<Car> carStream2 = cars.stream();
        Stream<Car> carStream3 = cars.stream();
        Stream<Car> carStream4 = cars.stream();

        System.out.println("Номера всех автомобилей имеющих черный цвет или нулевой пробег");
        carStream1
                .filter(car -> car.getColor().toLowerCase().equals("black") || car.getCar_mileage() == 0)
                .map(Car::getCar_number)
                .forEach(System.out::println);

        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700-800 тыс");
        long count = carStream2
                .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .distinct()
                .count();
        System.out.println(count);

        System.out.println("Вывести цвет автомобиля с минималной стоимостью");
        Optional<String> minCarColour = carStream3
                .min((Comparator.comparing(Car::getPrice)))
                .map(Car::getColor);
        System.out.println(minCarColour);

        System.out.println("Средняя стоимость камри");
        OptionalDouble averageCamry =  carStream4
                .filter(car -> car.getModel().toLowerCase().equals("camry"))
                .mapToInt(Car::getPrice)
                .average();
        System.out.println(averageCamry);
    }
}

