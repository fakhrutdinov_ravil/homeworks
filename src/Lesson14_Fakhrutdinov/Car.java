package Lesson14_Fakhrutdinov;

public class Car {
    private String car_number;
    private String model;
    private String color;
    private Integer car_mileage;
    private Integer price;

    public Car(String car_number, String model, String color, Integer car_mileage, Integer price) {
        this.car_number = car_number;
        this.model = model;
        this.color = color;
        this.car_mileage = car_mileage;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "car_number='" + car_number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", car_mileage=" + car_mileage +
                ", price=" + price +
                '}';
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCar_mileage() {
        return car_mileage;
    }

    public void setCar_mileage(Integer car_mileage) {
        this.car_mileage = car_mileage;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
