package Lesson14_Fakhrutdinov;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {
    //Перевод из cars.txt в обьекты Car
    public static List<Car> stringCarFromCar() {
        BufferedReader bufferedReader;
        String line;

        List<Car> carList = new ArrayList<>();

        Function<String, Car> getCarFromString = (Function<String, Car>) s -> {
            String[] parts = s.split("\\|");
            String car_number = parts[0];
            String car_name_factory = parts[1];
            String model = parts[2];
            String color = parts[3];
            String car_mileage = parts[4];
            String price = parts[5];
            return new Car(car_number, model, color, Integer.parseInt(car_mileage), Integer.parseInt(price));
        };

        try {
            bufferedReader = new BufferedReader(new FileReader("cars.txt"));
            line = bufferedReader.readLine();
            for (int lineCar = 0; line != null; lineCar++) {
                Car currentCar = getCarFromString.apply(line);
                carList.add(currentCar);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            throw new IllegalStateException("Файл не найден");
        }
        return carList;
    }

    public static void main(String[] args) {
        List<Car> cars = stringCarFromCar();
        Stream<Car> carStream = cars.stream();

        //Проверка на черный цвет ИЛИ нулевой пробег
        Predicate<Car> blackAreNullCarMilleage = car -> {
            if (car.getColor().toLowerCase().equals("black") || car.getCar_mileage() == 0) {
                return true;
            } else {
                return false;
            }
        };
        //Вызов toString у Car
        Consumer<String> printCar = car -> System.out.println(car);
        //Получение номера авто
        Function<Car, String> getNumberCar = car -> car.getCar_number();

        Stream<Car> filteredCarStream = carStream.filter(blackAreNullCarMilleage);
        Stream<String> numbersCars  = filteredCarStream.map(getNumberCar);
        numbersCars.forEach(printCar);
    }
}