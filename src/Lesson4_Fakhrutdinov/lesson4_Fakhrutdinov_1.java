package Lesson4_Fakhrutdinov;

public class lesson4_Fakhrutdinov_1 {
    public static void main(String[] args) {
        System.out.println(checkNumber(128));
    }

    public static boolean checkNumber(double n) {
        if (n == 1) {
            return true;
        }
        if (n < 1 || n==0) {
            return false;
        }
        return checkNumber(n / 2);
    }
}

