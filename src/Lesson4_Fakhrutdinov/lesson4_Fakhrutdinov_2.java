package Lesson4_Fakhrutdinov;

public class lesson4_Fakhrutdinov_2 {
    public static void main(String[] args) {
        int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(binarySearchRec(array, 11, 0, array.length - 1));
    }

    public static boolean binarySearchRec(int array[], int element, int from, int to) {
        int middle = from + (to - from) / 2;
        if (from <= to) {
            if (array[middle] > element) {
                return binarySearchRec(array, element, from, middle - 1);
            } else if (array[middle] < element) {
                return binarySearchRec(array, element, middle + 1, array.length - 1);
            } else if (array[middle] == element) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}


