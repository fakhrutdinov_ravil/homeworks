package Lesson12_Fakhrutdinov;

public interface Map<K, V> {
    void put(K var1, V var2);

    V get(K var1);
}
