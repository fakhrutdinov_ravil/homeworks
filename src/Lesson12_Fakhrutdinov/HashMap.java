package Lesson12_Fakhrutdinov;

public class HashMap<K, V> implements Map<K, V> {
    private static final int MAX_TABLE_SIZE = 8;
    private HashMap.MapEntry<K, V>[] table = new HashMap.MapEntry[8];

    public HashMap() {
    }

    public void put(K key, V value) {
        int hash = key.hashCode();
        int index = hash & this.table.length - 1;
        if (this.table[index] == null) {
            this.table[index] = new HashMap.MapEntry(key, value);
        } else {
            for(HashMap.MapEntry current = this.table[index]; current != null; current = current.next) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                } else {
                    current.next = new HashMap.MapEntry<K,V>(key, value);
                }
            }

            HashMap.MapEntry<K, V> newEntry = new HashMap.MapEntry(key, value);
            newEntry.next = this.table[index];
            this.table[index] = newEntry;
        }

    }

    public V get(K key) {
        int hash = key.hashCode();
        int index = hash & this.table.length - 1;
        return this.table[index].key.equals(key) ? this.table[index].value : null;
    }

    private static class MapEntry<K, V> {
        K key;
        V value;
        HashMap.MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
