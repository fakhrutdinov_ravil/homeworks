package Lesson7_Fakhrutdinov;

public class Main {
    public static class User {
        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public int getAge() {
            return age;
        }

        public boolean isWorker() {
            return isWorker;
        }

        public static class Builder {
            private User user;

            public Builder() {
                user = new User();
            }

            public Builder firstName(String firstName) {
                user.firstName = firstName;
                return this;
            }

            public Builder lastName(String lastName) {
                user.lastName = lastName;
                return this;
            }

            public Builder age(int age) {
                user.age = age;
                return this;
            }

            public Builder isWorker(Boolean isWorker) {
                user.isWorker = isWorker;
                return this;
            }

            public User build() {
                return user;
            }
        }

        @Override
        public String toString() {
            return "User{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age=" + age +
                    ", isWorker=" + isWorker +
                    '}';
        }
    }

    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();
        System.out.println(user.toString());
    }
}