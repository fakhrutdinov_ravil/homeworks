package Lesson1_Fakhrutdinov;

import java.util.Scanner;

public class lesson1_Fakhrutdinov_3 {
    public static void main(String[] args) {
        int number, copyNumber;
        int divisors;
        int count;
        int digitsSum;
        int summ = 1;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            number=scanner.nextInt();
            if (number==0) {
                break;
            }
            divisors = 0;
            digitsSum = 0;
            copyNumber = number;
            while (copyNumber % 10 != 0) {
                count = copyNumber % 10;
                copyNumber = copyNumber / 10;
                digitsSum = digitsSum + count;
            }
            for (int i = 1; i <= digitsSum; i++) {
                if (digitsSum % i == 0)
                    divisors++;
            }
            if (divisors <= 2) {
                summ = summ * number;
            }
        }
        System.out.println(summ);
    }
}