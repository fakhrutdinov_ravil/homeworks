package Lesson9_Fakhrutdinov;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int ints[] = new int[]{17, 13, 15, 11, 9};
        String strings[] = new String[]{"Russia2021","USA1990","Germany1808","A0u0s0t0ral0ia"};

        NumbersProcess reverseInt = (number) -> {
        String numberString = String.valueOf(number);
        String reverseNumber = new StringBuilder(numberString).reverse().toString();
        int reverseNumberInt = Integer.parseInt(reverseNumber);
        return reverseNumberInt;
    };
        NumbersProcess deleteZerosFromNumber = (number) -> {
            String numberString = String.valueOf(number);
            String withoutZero = numberString.replaceAll("0+","");
            return Integer.parseInt(withoutZero);
        };
        NumbersProcess replacementNumber = (number) -> {
            if (number%2 != 0) {
                number-=1;
            }
            return number;
        };

        StringProcess reverseString = (process) -> new StringBuilder(process).reverse().toString();
        StringProcess withoutZero = (process) -> process.replaceAll("0+","");
        StringProcess bigLetters = (process) -> process.toUpperCase();

        NumbersAndStringsProcessor numbersAndStringsProcessor = new NumbersAndStringsProcessor(strings, ints);

        int process[] = numbersAndStringsProcessor.process(reverseInt);
        System.out.println(Arrays.toString(process));

        int process1[] = numbersAndStringsProcessor.process(deleteZerosFromNumber);
        System.out.println(Arrays.toString(process1));

        int process2[] = numbersAndStringsProcessor.process(replacementNumber);
        System.out.println(Arrays.toString(process2));

        String process3[] = numbersAndStringsProcessor.process(reverseString);
        System.out.println(Arrays.toString(process3));

        String process4[] = numbersAndStringsProcessor.process(withoutZero);
        System.out.println(Arrays.toString(process4));

        String process5[] = numbersAndStringsProcessor.process(bigLetters);
        System.out.println(Arrays.toString(process5));

    }
}
