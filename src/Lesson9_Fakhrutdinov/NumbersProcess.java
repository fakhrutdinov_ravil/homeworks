package Lesson9_Fakhrutdinov;

@FunctionalInterface
public interface NumbersProcess {
    int process(int number);
}
