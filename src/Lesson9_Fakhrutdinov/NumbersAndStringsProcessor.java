package Lesson9_Fakhrutdinov;

import java.util.Arrays;

public class NumbersAndStringsProcessor {
    private String[] stringsArray;
    private int[] intsArray;

    public NumbersAndStringsProcessor(String[] stringsArray, int[] intsArray) {
        this.stringsArray = stringsArray;
        this.intsArray = intsArray;
    }

    int[] process(NumbersProcess process) {
        int[] ints = Arrays.copyOf(intsArray, intsArray.length);
        for (int index = 0; index < intsArray.length; index++) {
            ints[index] = process.process(intsArray[index]);
        }
        return ints;
    }

    String[] process(StringProcess process) {
        String[] strings = Arrays.copyOf(stringsArray, stringsArray.length);
        for (int index = 0; index < stringsArray.length; index++) {
            strings[index] = process.process(stringsArray[index]);
        }
        return strings;
    }
}

