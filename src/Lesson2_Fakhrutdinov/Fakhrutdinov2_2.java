package Lesson2_Fakhrutdinov;

import java.util.Scanner;
import java.util.Arrays;


public class Fakhrutdinov2_2 {
    public static void main(String args[]) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int positionOfMax = 0;
        int positionOfMin = 0;
        int temp;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
            if (array[i] > max) {
                max = array[i];
                positionOfMax = i;
            }
            if (array[i] < min) {
                min = array[i];
                positionOfMin = i;
            }
        }
        System.out.println(Arrays.toString(array));
        temp = array[positionOfMax];
        array[positionOfMax] = array[positionOfMin];
        array[positionOfMin] = temp;
        System.out.println("После перестановки: " + Arrays.toString(array));


    }
}
