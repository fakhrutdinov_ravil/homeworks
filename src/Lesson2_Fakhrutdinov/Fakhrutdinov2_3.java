package Lesson2_Fakhrutdinov;

import java.util.Arrays;

public class Fakhrutdinov2_3 {
    public static void main(String[] args) {
        int array[] = {45, 2235, 35, 51, 7};
        long number = 0;
        int arrayCopy[]= Arrays.copyOf(array, array.length);
        int rank;
        for (int i=0; i<array.length; i++) {
            rank=1;
            for (;arrayCopy[i]>0;arrayCopy[i]/=10) {
                rank*=10;
            }
            number=number*rank+array[i];
        }
        System.out.println(number);
    }
}