package Lesson2_Fakhrutdinov;

import java.util.Arrays;
import java.util.Scanner;

public class Fakhrutdinov2_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива M*N через Enter");
        int x = 0, y = 0, a=scanner.nextInt(), b=scanner.nextInt();
        int[][] array = new int[a][b];
        int number = 1;
        while (number <= a * b) {
            /* Выполняем проход по верхней строке  */
            while (x < array[0].length - 1) {
                if (array[y][x] <= 0) {
                    array[y][x] = number;
                    number++;
                }
                x++;
                /* Если в ячейке уже есть значение отходим налево и спускаемся ниже  */
                if (array[y][x] > 0) {
                    x--;
                    y++;
                    break;
                }
            }
            /* Выполняем проход по правому столбцу  */
            while (y < array.length - 1) {
                if (array[y][x] <= 0) {
                    array[y][x] = number;
                    number++;
                    y++;
                    /* Если в ячейке уже есть значение отходим налево и поднимаемся выше  */
                    if (array[y][x] > 0) {
                        x--;
                        y--;
                        break;
                    }
                }
            }
            /* Выполняем проход по нижнему столбцу  */
            while (x > 0) {
                if (array[y][x] <= 0) {
                    array[y][x] = number;
                    number++;
                    x--;
                }
                /* Если в ячейке уже есть значение отходим вправо и спускаемся ниже  */
                if (array[y][x] > 0) {
                    y--;
                    x++;
                    break;
                }
            }
            /* Выполняем проход по левому столбцу  */
            while (y > 0) {
                if (array[y][x] <= 0) {
                    array[y][x] = number;
                    number++;
                    y--;
                }
                /* Если в ячейке уже есть значение отходим вправо и спускаемся ниже  */
                if (array[y][x] > 0) {
                    x++;
                    y++;
                    break;
                }
            }
        }
        System.out.println(Arrays.deepToString(array));
    }
}