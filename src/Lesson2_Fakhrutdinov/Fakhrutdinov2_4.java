package Lesson2_Fakhrutdinov;

import java.util.Arrays;
import java.util.Scanner;

public class Fakhrutdinov2_4 {
    public static void main(String[] args) {
        int temp, max;
        Scanner scanner = new Scanner(System.in);
        int array[] = new int[scanner.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        max = array[0];
        for (int i = 0; i < (array.length - 1) * (array.length - 1); i++) {
            for (int n = 0; n < array.length - 1; n++) {
                if (array[n + 1] < array[n]) {
                    temp = array[n];
                    array[n] = array[n + 1];
                    array[n + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
