package MyGame.FinalExaminationExercise2.models;

import java.time.LocalTime;

public class Shoot {
    //Время выстрела
    private LocalTime timeOfShoot;
    //Попал/не попал
    private boolean hit = false;
    //В какой игре
    private Game game;
    //Кто в кого стрелял
    private Player shooter;
    private Player target;
}
