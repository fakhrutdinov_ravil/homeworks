package MyGame.FinalExaminationExercise3.models;

import java.time.LocalDate;
import java.time.LocalTime;

public class Game {
    //Дата
    private final LocalDate date = LocalDate.now();
    //Игроки
    private final Player firstPlayer;
    private final Player secondPlayer;
    //Сколько было выстрелов от игрока
    private Integer countShotFirstPlayer;
    private Integer countShotSecondPlayer;
    //Сколько длилась игра
    private LocalTime localTime;

    public Game(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }
}