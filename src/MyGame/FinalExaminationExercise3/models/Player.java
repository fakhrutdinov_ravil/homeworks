package MyGame.FinalExaminationExercise3.models;

import java.util.Objects;

public class Player {
    private Integer id;
    private String lastIp;
    private String nickName;
    private int maxScore;
    private int countOfWins;
    private int countOfFail;

    public Player(Integer id, String lastIp, String nickName, int maxScore, int countOfWins, int countOfFail) {
        this.id = id;
        this.lastIp = lastIp;
        this.nickName = nickName;
        this.maxScore = maxScore;
        this.countOfWins = countOfWins;
        this.countOfFail = countOfFail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public int getCountOfWins() {
        return countOfWins;
    }

    public void setCountOfWins(int countOfWins) {
        this.countOfWins = countOfWins;
    }

    public int getCountOfFail() {
        return countOfFail;
    }

    public void setCountOfFail(int countOfFail) {
        this.countOfFail = countOfFail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return maxScore == player.maxScore && countOfWins == player.countOfWins && countOfFail == player.countOfFail && Objects.equals(id, player.id) && Objects.equals(lastIp, player.lastIp) && Objects.equals(nickName, player.nickName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastIp, nickName, maxScore, countOfWins, countOfFail);
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", lastIp='" + lastIp + '\'' +
                ", nickName='" + nickName + '\'' +
                ", maxScore=" + maxScore +
                ", countOfWins=" + countOfWins +
                ", countOfFail=" + countOfFail +
                '}';
    }
}

