package MyGame.FinalExaminationExercise3.utils;

import MyGame.FinalExaminationExercise3.repositories.PlayersRepositoiry;

import java.io.*;

public class IdGeneratorFileBased implements IdGenerator {

    private String fileName;

    public IdGeneratorFileBased(String fileName) {
        this.fileName = fileName;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            try {
                if (reader.readLine() == null) {
                    writer.write("1");
                }
            } finally {
                writer.close();
                reader.close();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Integer nextId() {
        int currentId;
        try {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                currentId = Integer.parseInt(bufferedReader.readLine());
                ++currentId;
                try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
                    bufferedWriter.write(String.valueOf(currentId));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return currentId;
    }

    @Override
    public Integer getLastId() {
        try {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
                return Integer.parseInt(bufferedReader.readLine());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
