package MyGame.FinalExaminationExercise3;

import MyGame.FinalExaminationExercise3.models.Player;
import MyGame.FinalExaminationExercise3.models.PlayerBuilder;
import MyGame.FinalExaminationExercise3.repositories.PlayersRepositoiry;
import MyGame.FinalExaminationExercise3.repositories.PlayersRepositoryFilesImpl;
import MyGame.FinalExaminationExercise3.utils.IdGenerator;
import MyGame.FinalExaminationExercise3.utils.IdGeneratorFileBased;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        //Создаю ID генаратор
        IdGenerator idGenerator = new IdGeneratorFileBased("id.txt");
        //Создаю репозиторий игроков
        PlayersRepositoiry playersRepositoiry = new PlayersRepositoryFilesImpl("player.txt", idGenerator);
        //Создаю игроков
        Player player1 = new PlayerBuilder()
                .setNickName("Ravil")
                .setLastIp("192.168.0.1")
                .createPlayer();

        Player player2 = new PlayerBuilder()
                .setNickName("Anna")
                .setLastIp("192.168.0.2")
                .createPlayer();
        //Сохраняю игроков
        playersRepositoiry.save(player1);
        playersRepositoiry.save(player2);
        //Получаю игроков обратно уже с ID
        Optional playerOneWithId = playersRepositoiry.findByNickName("Ravil");
        Optional playerTwoWithID = playersRepositoiry.findByNickName("Anna");
        //Обновляю значение у игроков
        player1 = (Player) playerOneWithId.get();
        player2 = (Player) playerTwoWithID.get();
        player1.setCountOfFail(14);
        player2.setCountOfFail(16);
        //Обновляю игроков в репозитории
        playersRepositoiry.update(player1);
        playersRepositoiry.update(player2);
        //Поиск игрока по никнейму
        Player findPlayer = (Player) playersRepositoiry.findByNickName("Ravil").get();
        //Вывод статистики игрока в консоль
        System.out.println(findPlayer.toString());
    }
}
