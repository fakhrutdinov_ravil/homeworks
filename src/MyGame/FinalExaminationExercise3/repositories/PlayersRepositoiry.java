package MyGame.FinalExaminationExercise3.repositories;

import MyGame.FinalExaminationExercise3.models.Player;

import java.util.Optional;

public interface PlayersRepositoiry {
    void save(Player player);

    void update(Player player);

    Optional<Player> findByNickName(String nickname);

}
