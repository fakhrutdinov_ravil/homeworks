package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Game;
import ru.ravilgamedev.game.models.Player;
import ru.ravilgamedev.game.utils.*;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class GameRepositoryJdbcImpl implements GameRepository {

    private final PlayersRepositoiry playersRepositoiry;
    private final PlayersHitsRepository playersHitsRepository;
    private final DataSource dataSource;
    private final PlayersInGameRepository playersInGame;
    private final PlayersIdGetter playersIdGetter = new PlayersIdGetterImpl();

    public GameRepositoryJdbcImpl(PlayersRepositoiry playersRepositoiry, PlayersHitsRepository playersHitsRepository, DataSource dataSource, PlayersInGameRepository playersInGame) {
        this.playersRepositoiry = playersRepositoiry;
        this.playersHitsRepository = playersHitsRepository;
        this.dataSource = dataSource;
        this.playersInGame = playersInGame;
    }

    @Override
    public void save(Game game) {
        try (Connection dataSourceConnection = dataSource.getConnection()) {
            String sql;
            Long id = null;

            //Отправляем информацию о дате игры и времени игры.
            sql = "insert into game_repository(date_time, game_time) values (?,?)";
            PreparedStatement statement1 = dataSourceConnection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement1.setString(1, game.getDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            statement1.setString(2, game.getSecondGameTimeAmount().format(DateTimeFormatter.ofPattern("mm:ss")));
            statement1.execute();

            //Получаем и присваиваем ID.
            ResultSet resultSet = statement1.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            game.setId(id);

            //Сохранение количества попаданий в players_hits.
            playersHitsRepository.save(game);
            playersInGame.save(game);

            System.out.println("Игра успешно сохранена!");
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game getById(Long gameId) {

        Map<Player,Integer> playersHits;

        try (Connection connection = dataSource.getConnection()) {

            //Получаем информацию о дате игры и игрового времени.
            PreparedStatement statement = connection.prepareStatement("select * from game_repository where id=?");
            statement.setLong(1, gameId);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new IllegalArgumentException("Запрос не выполнен, игры с таким ID не существует");
            }

            //Релаизуем интерфейс для преобразования строки в LocalDateTime
            StringToLocalDateFormater toLocalDataFormater = dateTime -> {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                return LocalDateTime.parse(dateTime, formatter);
            };
            //Получаем время игры
            LocalDateTime gameDataTime = toLocalDataFormater.StringToLocalDataFormater(resultSet.getString("date_time"));

            //Релаизуем интерфейс для преобразования строки в LocalTime
            StringToLocalTimeFormater toLocalTimeFormater = dateTime -> LocalTime.parse(dateTime, DateTimeFormatter.ISO_LOCAL_TIME);
            //Получаем поле "Сколько длилась игра"
            LocalTime gameTime = toLocalTimeFormater.StringToLocalTimeFormater(resultSet.getString("game_time"));

            //Работаем с таблицей "Игроки в игре"
            //Получаем список игроков у которых game_id = gameID
            List<Player> playerList = playersInGame.getPlayersInGame(gameId);

            //Работаем с таблицей "Количество попаданий каждого игрока"
            playersHits = playersHitsRepository.returnAllPlayersHitsById(gameId);

            return Game.builder()
                    .id(gameId)
                    .dateTime(gameDataTime)
                    .players(playerList)
                    .playersHits(playersHits)
                    .secondGameTimeAmount(gameTime)
                    .build();

        } catch (
                SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(Game game) {
    }

    @Override
    public Long getId() {
        Long lastGeneratedId = null;
        String sql = "select * from game_repository";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.executeQuery();
            statement.getGeneratedKeys();
            while (statement.getResultSet().next()) {
                lastGeneratedId = statement.getResultSet().getLong("id");
            }
            return lastGeneratedId;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
