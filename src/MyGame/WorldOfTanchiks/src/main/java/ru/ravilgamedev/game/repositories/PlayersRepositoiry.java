package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Player;

import java.util.Optional;

public interface PlayersRepositoiry<P> extends CrudRepository<Player> {
    Optional<Player> findByNickName(String nickname);
    Player getByNickName(String nickname);
    Player getById(Long playerId);
}
