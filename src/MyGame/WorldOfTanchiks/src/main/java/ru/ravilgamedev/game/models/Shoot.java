package ru.ravilgamedev.game.models;

import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Shoot {
    private Long id;
    //Время выстрела
    private LocalDateTime timeOfShoot;
    //В какой игре
    private Game game;
    //Кто в кого стрелял
    private Player shooter;
    private Player target;

}
