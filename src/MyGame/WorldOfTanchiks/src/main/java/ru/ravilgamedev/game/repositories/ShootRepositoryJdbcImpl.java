package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Shoot;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.Set;

public class ShootRepositoryJdbcImpl implements ShootRepository {

    private final DataSource dataSource;

    public ShootRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shoot shoot) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into shoot_repository(time_of_shoot, game_id, shooter_id, target_id)values (?,?,?,?)");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            preparedStatement.setString(1, shoot.getTimeOfShoot().format(formatter));
            preparedStatement.setLong(2,shoot.getGame().getId());
            preparedStatement.setLong(3,shoot.getShooter().getId());
            preparedStatement.setLong(4,shoot.getTarget().getId());
            if (preparedStatement.executeUpdate() > 0) {
                System.out.println("Сохранение выстрела в shooter_repository прошло успешно!");
            } else {
                throw new IllegalStateException("Сохранение выстрела не произошло, ошибка!");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
