package ru.ravilgamedev.game.repositories;

import java.util.List;

public interface CrudRepository<T> {
    default void save(T t) {
        throw new UnsupportedOperationException("Must be implemented if used");
    }

    default void update(T t) {
        throw new UnsupportedOperationException("Must be implemented if used");
    }

    default List<T> findAll(T t) {
        throw new UnsupportedOperationException("Must be implemented if used");
    }

    default boolean delete(T t) {
        throw new UnsupportedOperationException("Must be implemented if used");
    }
}
