package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Game;
import ru.ravilgamedev.game.models.Player;

public interface GameRepository extends CrudRepository<Game> {
    Game getById(Long gameId);
    Long getId();
}
