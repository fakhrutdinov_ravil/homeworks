package ru.ravilgamedev.game.models;

import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Game {
    //ID игры
    private Long id;
    //Дата
    private LocalDateTime dateTime;
    //Игроки
    private List<Player> players;
    //Сколько было попаданий от игроков
    private Map<Player, Integer> playersHits;
    //Сколько длилась игра
    private LocalTime secondGameTimeAmount;
}