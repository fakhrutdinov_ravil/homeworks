package ru.ravilgamedev.game;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.ravilgamedev.game.models.Player;
import ru.ravilgamedev.game.repositories.*;
import ru.ravilgamedev.game.services.GameService;
import ru.ravilgamedev.game.services.GameServiceImpl;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class MainGame {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/WorldOfTanchiksDatabase");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("admin");
        hikariConfig.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(hikariConfig);

        PlayersRepositoiry<Player> playerRepositoryJdbc = new PlayersRepositoiryJdbcImpl(dataSource);
        PlayersHitsRepository playersHitsRepositoryJdbc = new PlayersHitsRepositoryJdbcImpl(dataSource,playerRepositoryJdbc);
        PlayersInGameRepository playersInGameRepositoryJdbc = new PlayersInGameRepositoryImpl(dataSource,playerRepositoryJdbc);
        GameRepository gameRepositoryJdbc = new GameRepositoryJdbcImpl(playerRepositoryJdbc,playersHitsRepositoryJdbc,dataSource,playersInGameRepositoryJdbc);
        ShootRepository shootRepository = new ShootRepositoryJdbcImpl(dataSource);

        GameService gameService = new GameServiceImpl(playerRepositoryJdbc, gameRepositoryJdbc, shootRepository, playersHitsRepositoryJdbc);


        Map<String,String> players = new HashMap<>();
        players.put("192.168.0.1","Ravil");
        players.put("192.168.0.2","Anna");

        Long idGame = gameService.startGame(players);
        gameService.shoot(idGame,"Ravil","Anna");


    }
}
