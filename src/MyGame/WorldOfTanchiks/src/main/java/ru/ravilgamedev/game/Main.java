//package ru.ravilgamedev.game;
//
//import ru.ravilgamedev.game.models.*;
//import ru.ravilgamedev.game.utils.*;
//import ru.ravilgamedev.game.repositories.*;
//
//import java.util.Optional;
//
//import static ru.ravilgamedev.game.models.Player.*;
//
//public class Main {
//    public static void main(String[] args) {
//        //Создаю ID генаратор
//        IdGenerator idGenerator = new IdGeneratorFileBased("id.txt");
//        //Создаю репозиторий игроков
////        PlayersRepositoiry playersRepositoiry = new PlayersRepositoryFilesImpl("player.txt", idGenerator);
//        //Создаю игроков
//        Player player1 = builder()
//                .nickName("Ravil")
//                .build();
//
//        Player player2 = builder()
//                .nickName("Anna")
//                .lastIp("192.168.0.2")
//                .build();
//        //Сохраняю игроков
//        playersRepositoiry.save(player1);
//        playersRepositoiry.save(player2);
//        //Получаю игроков обратно уже с ID
//        Optional playerOneWithId = playersRepositoiry.findByNickName("Ravil");
//        Optional playerTwoWithID = playersRepositoiry.findByNickName("Anna");
//        //Обновляю значение у игроков
//        player1 = (Player) playerOneWithId.get();
//        player2 = (Player) playerTwoWithID.get();
//        player1.setCountOfFail(14);
//        player2.setCountOfFail(16);
//        //Обновляю игроков в репозитории
//        playersRepositoiry.update(player1);
//        playersRepositoiry.update(player2);
//        //Поиск игрока по никнейму
//        Player findPlayer = (Player) playersRepositoiry.findByNickName("Ravil").get();
//        //Вывод статистики игрока в консоль
//        System.out.println(findPlayer.toString());
//    }
//}
