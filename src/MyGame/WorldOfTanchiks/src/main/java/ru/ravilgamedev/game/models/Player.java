package ru.ravilgamedev.game.models;

import lombok.Builder;

import java.util.Objects;
@Builder
public class Player {
    private Long id;
    private String ip;
    private String nickName;
    private int maxCount;
    private int countOfWins;
    private int countOfFail;

    public Player(Long id, String ip, String nickName, int points, int countOfWins, int countOfFail) {
        this.id = id;
        this.ip = ip;
        this.nickName = nickName;
        this.maxCount = points;
        this.countOfWins = countOfWins;
        this.countOfFail = countOfFail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCountOfWins() {
        return countOfWins;
    }

    public void setCountOfWins(int countOfWins) {
        this.countOfWins = countOfWins;
    }

    public int getCountOfFail() {
        return countOfFail;
    }

    public void setCountOfFail(int countOfFail) {
        this.countOfFail = countOfFail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return maxCount == player.maxCount && countOfWins == player.countOfWins && countOfFail == player.countOfFail && Objects.equals(id, player.id) && Objects.equals(ip, player.ip) && Objects.equals(nickName, player.nickName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ip, nickName, maxCount, countOfWins, countOfFail);
    }
}



