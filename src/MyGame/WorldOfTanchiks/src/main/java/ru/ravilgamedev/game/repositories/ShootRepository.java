package ru.ravilgamedev.game.repositories;

import ru.ravilgamedev.game.models.Shoot;

public interface ShootRepository extends CrudRepository<Shoot> {

}
