package Lesson10_Fakhrutdinov;

public interface List extends Collection {

    int get(int index);

    void removeByIndex(int index);
}
