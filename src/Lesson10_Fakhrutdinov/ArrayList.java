package Lesson10_Fakhrutdinov;

public class ArrayList implements List {

    private final int DEFAULT_ARRAY_SIZE = 10;
    private int elements[];
    private int size;

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.size = 0;
    }

    @Override
    public void add(int element) {
        if (size == elements.length) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    @Override
    public boolean contains(int element) {
        for (int index = 0; index < elements.length; index++) {
            if (elements[index] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void remove(int element) {
        for (int index = 0; index < elements.length; index++) {
            if (elements[index] == element) {
                System.arraycopy(elements, index + 1, elements, index, size-(index+1));
            }
        }
        size--;
    }

    @Override
    public int get(int index) {
        if (index < 0 || index > size) {
            System.err.println("Индекс не входит в границы списка");
        }
        return elements[index];
    }

    @Override
    public void removeByIndex(int index) {
        System.arraycopy(elements, index + 1, elements, index, size - (index + 1));
        size--;
    }

    public void resize() {
        int newArray[] = new int[elements.length + elements.length / 2];
        for (int index = 0; index < elements.length; index++) {
            newArray[index] = elements[index];
        }
        this.elements = newArray;
    }

    public class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int element = elements[current];
            current++;
            return element;
        }

        @Override
        public boolean hasNext() {
            return current < size;
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
