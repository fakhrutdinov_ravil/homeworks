package Lesson10_Fakhrutdinov;

public interface Iterator {

    int next();

    boolean hasNext();
}
